From d81f561923c9b88d7da4add7b08a1e8b6cbfa9f8 Mon Sep 17 00:00:00 2001
From: Michal Simek <michal.simek@amd.com>
Date: Fri, 23 Feb 2024 17:18:16 +0100
Subject: [PATCH 9/9] arm64: zynqmp: Add MTD partition handling for
 dfu_alt_info generation

Generate dfu_alt_info generation based on information from MTD partitions.
mtd_found_part() is trying to identify MTD partition which code is running
from. If partitions are not defined and location is not found it is going
to previous behavior.

Signed-off-by: Michal Simek <michal.simek@amd.com>
Signed-off-by: Javier Tia <javier.tia@linaro.org>

Upstream-Status: Pending
---
 board/xilinx/zynqmp/zynqmp.c | 54 ++++++++++++++++++++++++++++++------
 1 file changed, 46 insertions(+), 8 deletions(-)

diff --git a/board/xilinx/zynqmp/zynqmp.c b/board/xilinx/zynqmp/zynqmp.c
index 9f50090720..d56089e8b4 100644
--- a/board/xilinx/zynqmp/zynqmp.c
+++ b/board/xilinx/zynqmp/zynqmp.c
@@ -625,6 +625,31 @@ enum env_location env_get_location(enum env_operation op, int prio)
 
 #define DFU_ALT_BUF_LEN		SZ_1K
 
+static void mtd_found_part(u32 *base, u32 *size)
+{
+	struct mtd_info *part, *mtd;
+
+	mtd_probe_devices();
+
+	mtd = get_mtd_device_nm("nor0");
+	if (!IS_ERR_OR_NULL(mtd)) {
+		list_for_each_entry(part, &mtd->partitions, node) {
+			debug("0x%012llx-0x%012llx : \"%s\"\n",
+			      part->offset, part->offset + part->size,
+			      part->name);
+
+			if (*base >= part->offset &&
+			    *base < part->offset + part->size) {
+				debug("Found my partition: %d/%s\n",
+				      part->index, part->name);
+				*base = part->offset;
+				*size = part->size;
+				break;
+			}
+		}
+	}
+}
+
 void set_dfu_alt_info(char *interface, char *devstr)
 {
 	int multiboot, bootseq = 0, len = 0;
@@ -666,15 +691,28 @@ void set_dfu_alt_info(char *interface, char *devstr)
 		break;
 	case QSPI_MODE_24BIT:
 	case QSPI_MODE_32BIT:
-		len += snprintf(buf + len, DFU_ALT_BUF_LEN,
-			       "sf 0:0=boot.bin raw %x 0x1500000",
-			       multiboot * SZ_32K);
-#if defined(CONFIG_SPL_FS_LOAD_PAYLOAD_NAME) && defined(CONFIG_SYS_SPI_U_BOOT_OFFS)
-		len += snprintf(buf + len, DFU_ALT_BUF_LEN,
-			       ";%s raw 0x%x 0x500000",
-			       CONFIG_SPL_FS_LOAD_PAYLOAD_NAME,
-			       multiboot * SZ_32K + CONFIG_SYS_SPI_U_BOOT_OFFS);
+		{
+			u32 base = multiboot * SZ_32K;
+			u32 size = 0x1500000;
+			u32 limit = size;
+
+			mtd_found_part(&base, &limit);
+
+#if defined(CONFIG_SYS_SPI_U_BOOT_OFFS)
+			size = limit;
+			limit = CONFIG_SYS_SPI_U_BOOT_OFFS;
 #endif
+
+			len += snprintf(buf + len, DFU_ALT_BUF_LEN,
+					"sf 0:0=boot.bin raw 0x%x 0x%x",
+					base, limit);
+#if defined(CONFIG_SPL_FS_LOAD_PAYLOAD_NAME) && defined(CONFIG_SYS_SPI_U_BOOT_OFFS)
+			len += snprintf(buf + len, DFU_ALT_BUF_LEN,
+					";%s raw 0x%x 0x%x",
+					CONFIG_SPL_FS_LOAD_PAYLOAD_NAME,
+					base + limit, size - limit);
+#endif
+		}
 		break;
 	default:
 		return;
-- 
2.44.0

