# Generate U-Boot for imx8mp verdin
inherit deploy

FILESEXTRAPATHS:prepend := "${THISDIR}/u-boot/${MACHINE}:"

SRC_URI += "file://${MACHINE}.cfg"

UBOOT_BOARDDIR = "${KCONFIG_CONFIG_ROOTDIR}"
UBOOT_ENV_NAME = "imx8mp-verdin.env"

DEPENDS:verdin-imx8mp += "trusted-firmware-a imx-boot-firmware-files"

do_compile:prepend() {
    cp ${DEPLOY_DIR_IMAGE}/lpddr4_pmu_train_*.bin "${KCONFIG_CONFIG_ROOTDIR}"
    cp ${DEPLOY_DIR_IMAGE}/signed_*_imx8m.bin "${KCONFIG_CONFIG_ROOTDIR}"

    cp ${DEPLOY_DIR_IMAGE}/bl31.bin "${KCONFIG_CONFIG_ROOTDIR}"
}

do_deploy:append() {
    cp "${KCONFIG_CONFIG_ROOTDIR}/flash.bin" "${DEPLOYDIR}"
}

ATF_DEPENDS = " trusted-firmware-a:do_deploy"
FW_DEPENDS = " imx-boot-firmware-files:do_deploy"
do_compile[depends] .= "${ATF_DEPENDS} ${FW_DEPENDS}"
