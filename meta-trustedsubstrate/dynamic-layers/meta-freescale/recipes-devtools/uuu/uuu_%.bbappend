DEPENDS += "upx-native"

inherit deploy

EXTRA_OECMAKE += "-DSTATIC=1"

addtask deploy before do_build after do_compile

do_deploy() {
    install -D -p -m0755 ${B}/uuu/uuu ${DEPLOY_DIR_IMAGE}/uuu
    upx --best --lzma ${DEPLOY_DIR_IMAGE}/uuu
}
